(ns nva.core
  (:require [clj-http.client :as client]
            [clojure.xml :as xml]
            [hickory.core :as h]
            [hickory.select :as s]
            [clojure.pprint :as p]
            [clojure.data.json :as json]
            [clojure.edn :as edn])
  (:gen-class))

(def url "http://www.nouvelle-vague.com/evenements/")

(defn entete [v]
  (str "<ASCII-MAC>\n" v "<FeatureSet:InDesign-Roman><ColorTable:=<Black:COLOR:CMYK:Process:0,0,0,1><2-ORANGE-zoom:COLOR:CMYK:Process:0,0.67,1,0><2-JAUNE-zoom:COLOR:CMYK:Process:0,0.1,1,0><carr<0x00E9> rouge:COLOR:CMYK:Process:0.02,1,1,0.33>>
  <DefineCharStyle:AGENDA\\:AGENDA-TITRE mois=<Nextstyle:AGENDA\\:AGENDA-TITRE mois><cColor:Black><cTypeface:Regular><cSize:12.000000><cCase:All Caps><cFont:Charifa SerifLight>>
  <DefineCharStyle:AGENDA\\:AGENDA-date=<Nextstyle:AGENDA\\:AGENDA-date><cColor:2-ORANGE-zoom><cTypeface:67 Medium Condensed><cSize:8.000000><cLeading:9.500000><cFont:Helvetica Neue LT Pro>>
  <DefineCharStyle:AGENDA\\:AGENDA-artiste=<Nextstyle:AGENDA\\:AGENDA-artiste><cColor:carr<0x00E9> rouge><cTypeface:57 Condensed><cSize:8.000000><cTracking:-10><cCase:Normal><cLeading:9.500000><cFont:Helvetica Neue LT Pro>>
  <DefineCharStyle:AGENDA\\:AGENDA-lieu=<Nextstyle:AGENDA\\:AGENDA-lieu><cTypeface:47 Light Condensed><cSize:8.000000><cTracking:-5><cLeading:9.500000><cFont:Helvetica Neue LT Pro>>
  <DefineCharStyle:AGENDA\\:AGENDA-departement=<Nextstyle:AGENDA\\:AGENDA-departement><cColor:Black><cTypeface:67 Medium Condensed><cSize:8.000000><cTracking:-5><cLeading:9.500000><cFont:Helvetica Neue LT Pro><cColorTint:100.000000>>
  <DefineParaStyle:AGENDA\\:AGENDA-mois=<Nextstyle:AGENDA\\:AGENDA-mois><cCase:All Caps><pHyphenation:0><cFont:Charifa SerifLight><pRuleAboveColor:Black><pRuleAboveStroke:0.250000><pRuleAboveOffset:17.007874><pRuleBelowColor:Black><pRuleBelowStroke:0.250000><pRuleBelowOffset:5.669291><pRuleAboveOn:1><pRuleBelowOn:1><pRunInStyles:AGENDA\\:AGENDA-date\\,RI digit\\,2\\,1\\;AGENDA\\:AGENDA-artiste\\,RI nspace\\,1\\,0\\;AGENDA\\:AGENDA-lieu\\,RI sentence\\,1\\,0\\;><pTextAlignment:Center><pRuleAboveKeepInFrame:1><pGridAlign:BaseLine>>
  <DefineParaStyle:AGENDA\\:AGENDA=<Nextstyle:AGENDA\\:AGENDA><cTypeface:47 Light Condensed><cSize:8.500000><cLeading:9.800000><pHyphenation:0><cFont:Helvetica Neue LT Pro><pRunInStyles:AGENDA\\:AGENDA-date\\,RI digit\\,2\\,1\\;AGENDA\\:AGENDA-artiste\\,RI nspace\\,1\\,0\\;AGENDA\\:AGENDA-lieu\\,\\(\\,1\\,0\\;AGENDA\\:AGENDA-departement\\,\\)\\,1\\,1\\;AGENDA\\:AGENDA-lieu\\,RI sentence\\,1\\,0\\;>>\n"))


(defn get-page [url]
  (-> (client/get url)
    :body
    h/parse
    h/as-hickory))

(defn encode-non-ascii [c]
  (let [i (int c)]
    (if (> i 127) (str "<0x" (clojure.string/upper-case (format "%04x" i)) ">") c)))

(defn v->hm [v]
  (->> (:content v)
    (filter (comp not string?))
    (map :content)
    (map (partial filter (comp not string?)))
    (map (fn [[date groupe lieu ville departement heure]]
           (let [cf #(-> % :content first)]
             {:date (-> date cf cf)
              :groupe (-> groupe cf cf cf)
              :lieu (-> lieu cf cf)
              :ville (-> ville cf cf)
              :departement (-> departement cf cf)
              :heure (-> heure cf cf)})))
    distinct))

(defn clean-kv [els]
  (let [kvs (partition 2 els)]
    (mapcat (fn [[k v]] [(-> k :content first)
                         (v->hm v)]) kvs)))

(defn do-partner-style [e-str]
  (str "<cUnderline:1><cUnderlineColor:2-JAUNE-zoom><cUnderlineOffset:-3.000000><cUnderlineWeightOffset:6.000000>"
    e-str
    "<cUnderline:><cUnderlineColor:><cUnderlineOffset:><cUnderlineWeightOffset:>"))

(defn event->tagged-str [{:keys [date groupe lieu ville departement heure partner]}]
  (let [jour (first (re-seq #"\w+" (or date "")))
        dep (first (re-seq #"\w+" (or departement "")))
        dep (str "(" dep ")")
        ascii #(apply str (map encode-non-ascii %))
        lieu (if (= lieu "Nom de la salle") "à " (str lieu " - "))
        e-str
        (str
          (ascii jour) "  "
          (ascii groupe) " <0x2002> "
          (ascii lieu)
          (ascii ville) "  "
          (ascii dep) "  "
          (ascii heure))]
    (str "<ParaStyle:AGENDA\\:AGENDA>"
      (if partner (do-partner-style e-str) e-str)
      "\n")))

(defn month->tagged-str [m]
  (let [[m y] (re-seq #"[^,\s]+" m)
        m (clojure.string/capitalize m)
        s (str m " " y)
        ascii-str (apply str (map encode-non-ascii s))]
    (str "\n\n\n\n<ParaStyle:AGENDA\\:AGENDA-mois><CharStyle:AGENDA\\:AGENDA-TITRE mois>"
      ascii-str
      "\n<CharStyle:><ParaStyle:AGENDA\\:AGENDA>\n\n")))

(def month->int
  {"janvier" 1
   "février" 2
   "mars" 3
   "avril" 4
   "mai" 5
   "juin" 6
   "juillet" 7
   "août" 8
   "septembre" 9
   "octobre" 10
   "novembre" 11
   "décembre" 12})

(defn agenda
  ([]
   (loop [n 1 r {}]
     (if-let [a (seq (agenda n))]
       (recur (inc n) (merge-with concat r a))
       r)))
  ([n]
   (let [page (get-page (str url "?pno=" n))
         els (clean-kv (s/select (s/or (s/tag :h2) (s/tag :tbody)) page))]
     (apply hash-map els))))

(defn k->comparable
  [k]
  (let [[m y] (clojure.string/split k #" ")
        m (apply str (butlast (seq m)))]
    [(Integer. y) (get month->int m)]))

(defn hm->seq [hm]
  (apply concat (seq hm)))

(defn sorted-agenda [a]
  (apply sorted-map-by
    #(compare (k->comparable %1) (k->comparable %2))
    (hm->seq a)))

(defn add-partner-tag [p-hm a]
  (let [date->vec
        #(mapv
          (fn [x] (Integer. x))
          (reverse (re-seq #"\w+" %)))
        in-date-range?
        #(and (>= (compare %1 %2) 0)
          (<= (compare %1 %3) 0))]
    (apply hash-map
      (mapcat
        (fn [[k v]]
          [k
           (map
             (fn [x]
               (if-let [[from to] (get p-hm (clojure.string/lower-case (:lieu x)))]
                 (if (apply in-date-range? (map date->vec [(:date x) from to]))
                   (assoc x :partner true)
                   x)
                 x))
             v)])
        a))))

(defn agenda->tagged-txt [a partners version]
  (let [partners
        (apply hash-map
          (mapcat
            (fn [[k v]]
              [(clojure.string/lower-case k) v])
            partners))]
    (->> a
      (add-partner-tag partners)
      sorted-agenda
      hm->seq
      (partition 2)
      (mapcat
        (fn [[k v]]
          [(month->tagged-str k)
           (apply str (mapv event->tagged-str v))]))
      (apply str (entete version)))))

(defn write-agenda-edn []
  (with-open [w (clojure.java.io/writer "src/nva/data/agenda.edn")]
    (binding [*out* w]
      (clojure.pprint/write (agenda)))))

(defn make-txt []
  (spit
    "src/nva/data/agenda.txt"
    (agenda->tagged-txt
      (edn/read-string (slurp "src/nva/data/agenda.edn"))
      (edn/read-string (slurp "src/nva/data/partners.edn"))
      "<Version:8>")))



