(ns nva.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :refer [site]]
            [clojure.java.io :as io]
            [ring.middleware.reload :as reload]
            [environ.core :refer [env]]
            [nva.core :refer [make-txt write-agenda-edn]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.util.response :refer [redirect]]
            [hiccup.core :refer [html]]
            [org.httpkit.server :as http])
  (:gen-class))

(defn index-page []
  (html
    [:head
     [:link {:rel "stylesheet" :href "main.css"}]
     [:link {:rel "stylesheet" :href "anim.css"}]
     [:link {:rel "stylesheet" :href "fontello/css/fontello.css"}]]
    [:body
     [:h1 "Nouvelle Vague"]
     [:h2 "Agenda Web Service"]
     [:div.action-wrap [:div.action " agenda.edn "
                        [:a.icon-dl.dl-edn {:href "/agenda-edn"}]
                        [:a.icon-refresh.refresh-edn]
                        [:span.wait {:style "display:none"} "loading..."]]]
     [:div.action-wrap [:div.action " agenda.txt "
                        [:a.icon-dl.dl-txt {:href "/agenda-txt"}]
                        [:a.icon-refresh.refresh-txt]
                        [:span.wait {:style "display:none"} "loading..."]]]
     [:div.action-wrap [:div.action " partners.edn "
                        [:a.icon-dl {:href "/partners"}]
                        [:a.icon-ul {:href "javascript:void(0)"
                                     :onclick "document.querySelectorAll('.upload-wrap')[0].style.display = 'inline-block'"}]]]
     [:div.upload-wrap
      [:form {:action "/upload" :method "post" :enctype "multipart/form-data"}
       [:input {:name "file" :type "file" :size "20"}]
       [:input.submit {:type "submit" :name "submit" :value "submit"}]]]
     [:script {:src "main.js"}]]))

(defn send-file [path as]
  {:status 200
   :headers {"Content-Type" "text/plain; charset=utf-8"
             "Content-Disposition" (str "attachment; filename=" as)}
   :body (slurp path)})

(defn action-socket [req]
  (http/with-channel req channel
    (http/on-close channel
      (fn [status]
        (println "channel closed")))
    (http/on-receive channel
      (fn [action]
        (case action
          "refresh-edn"
          (future
            (do (write-agenda-edn)
                (make-txt)
                (http/send! channel "edn")))
          "refresh-txt"
          (future
            (do (make-txt)
                (http/send! channel "txt")))
          "tic" nil)))))

(defroutes app-routes

  (GET "/" []
    (index-page))

  (GET "/action" [] action-socket)

  (GET "/agenda-edn" []
    (send-file "src/nva/data/agenda.edn" "agenda.edn"))

  (GET "/agenda-txt" []
    (send-file "src/nva/data/agenda.txt" "agenda.txt"))

  (GET "/partners" []
    (send-file
      "src/nva/data/partners.edn"
      "partners.edn"))

  (POST "/upload"
        {{{tempfile :tempfile filename :filename} :file} :params}
    (io/copy tempfile (io/file "src" "nva" filename))
    (redirect "/"))

  (route/not-found "Not Found"))


(defn -main [& [port]]
  (let [port (Integer. (or port (env :port) 5000))]
    (http/run-server
      (wrap-defaults app-routes (assoc-in site-defaults [:security :anti-forgery] false))
      {:port port
       ;:join? false
       })))

(comment "dev"
  (defn -main [& [port]]
    (let [port (Integer. (or port (env :port) 5000))]
      (http/run-server
        (reload/wrap-reload (wrap-defaults app-routes (assoc-in site-defaults [:security :anti-forgery] false)))
        {:port port}))))