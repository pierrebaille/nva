(ns nva.style
  (:require [garden.core :refer [css]]))

(let [pad "10px"
      marg "10px"
      c1 "#64AECB"
      bord "3px solid #8BC1D5"
      br "5px"
      tcol "white"]
  (css {:output-to "resources/public/main.css"}
    [:body {:background-color "rgba(245, 240, 236, 0.45)"
            :padding "10px"}]
    [:h1 :h2
     {:padding (str 0 pad)
      :color c1}]
    [:h1 {:margin-bottom 0}]
    [:h2 {:margin-top 0}]
    [:.action-wrap
     {:margin marg}]
    [:.action
     {:width "292px"
      :display :inline-block
      :background c1
      :border bord
      :border-radius br
      :padding pad
      :text-decoration :none
      :color tcol
      :font-size "20px"}
     [:a
      {:color tcol
       :text-decoration :none
       :float :right}]]
    [:span.wait {:color tcol :float :right}]
    [:.upload-wrap
     [:form {:color tcol
             :padding pad
             :margin 0}]
     [:input.submit {:margin-left "8px"}]
     {:border bord
      :border-radius br
      :display :none
      :background "#8BC1D5"
      :margin (str 0 " " marg " " marg " " marg)
      :font-size "20px"}]))
