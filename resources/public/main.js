

document.addEventListener("DOMContentLoaded", function(event) {

  var dlEdn = document.querySelectorAll('.dl-edn')[0];
  var dlTxt = document.querySelectorAll('.dl-txt')[0];
  var rfEdn = document.querySelectorAll('.refresh-edn')[0];
  var rfTxt = document.querySelectorAll('.refresh-txt')[0];
  var waits = document.querySelectorAll('.wait');

  // prod
  var socket = new WebSocket("wss://nvaws.herokuapp.com/action");
  // dev
  // var socket = new WebSocket("ws://localhost:5000/action");

  socket.onopen = function()  {
        console.log('websocket opened');
        setInterval(function() {
          if (socket.bufferedAmount == 0)
            socket.send("tic");
          }, 30000 );
     };

  socket.onmessage = function(m) {
    console.log(m)
    switch (m.data) {
    case "edn": {
      dlEdn.style.display = 'inline-block';
      rfEdn.style.display = 'inline-block';
      waits[0].style.display = 'none'}
      break;
    case "txt": {
      dlTxt.style.display = 'inline-block';
      rfTxt.style.display = 'inline-block';
      waits[1].style.display = 'none'}
      break;
  };};

  rfEdn.onclick= function(){
    socket.send("refresh-edn");
    dlEdn.style.display = 'none';
    rfEdn.style.display = 'none';
    waits[0].style.display = 'inline';}

  rfTxt.onclick= function(){
    socket.send("refresh-txt");
    dlTxt.style.display = 'none';
    rfTxt.style.display = 'none';
    waits[1].style.display = 'inline';}
});
