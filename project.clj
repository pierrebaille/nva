(defproject nva "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.3.1"]
                 [ring "1.4.0-RC1"]
                 [ring/ring-defaults "0.1.2"]
                 #_[ring/ring-jetty-adapter "1.2.2"]
                 [clj-time "0.6.0"]
                 [environ "0.5.0"]
                 [clj-http "1.1.2"]
                 [hickory "0.5.4"]
                 [org.clojure/data.json "0.2.6"]
                 [hiccup "1.0.5"]
                 [garden "1.2.5"]
                 [http-kit "2.1.18"]]
  :ring {:handler nva.handler/app
         :auto-refresh? true}
  :plugins [[environ/environ.lein "0.2.1"]
            [lein-ring "0.8.13"]]
  :hooks [environ.leiningen.hooks]
  :uberjar-name "nva.jar"
  :main nva.handler
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}
   :production {:env {:production true}}})
